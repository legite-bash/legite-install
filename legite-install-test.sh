#!/bin/bash
# legite-install-test.sh
# programme pour faire des test sur le programme
set -u;
#declare -r scriptPath=`realpath $0`;         # chemin  absolu complet du script rep + nom
#declare -r scriptRep=`dirname $scriptPath`;  # repertoire absolu du script (pas de slash de fin)
#declare -r scriptFileName=${0##*/};          # nom.ext
#declare -r scriptNom=${scriptFileName%.*}    # uniquement le nom
declare -r PROG_NOM="/www/bash/legite-install/legite-install.sh";


################
# colorisation #
################
RESET_COLOR=$(tput sgr0)
BOLD=`tput smso`
NOBOLD=`tput rmso`

BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
CYAN=`tput setaf 4`
MAGENTA=`tput setaf 5`
BLUE=`tput setaf 6`
WHITE=`tput setaf 7`

NORMAL=$WHITE
INFO=$BLUE
CMD=$YELLOW
WARN=$RED
TITRE1=$GREEN
TITRE2=$MAGENTA
DEBUG_COLOR=$MAGENTA


####################
# CODES DE SORTIES #
####################
declare -r E_ARG_NONE=65


#########
# DEBUG #
#########
#evalCmd($cmd $ligneNu $txt)
evalCmd(){
    local ligneNu="";   if [ ! -z ${2+x} ]; then ligneNu="[$2]:"; fi
    local txt="";       if [ ! -z ${3+x} ]; then txt="$3"; fi

    echo "$ligneNu$CMD$1$NORMAL $txt";
    eval "$1";
    return $?
}


#showDebug(texte)
showDebug(){
if [ $iSDebug -eq 1 ];
then
    local texte="$1";
    if [ $# -eq 2 ]; then
        texte=$2;
    fi
    echo "${DEBUG_COLOR}$texte$NORMAL"
fi
}


########
# MAIN #
########
evalCmd"cd /tmp" #pour tester hors repertoire du programme et proteger en cas d'erreur

echo "Programme pour tester le programme $PROG_NOM"

clear;
echo "#############################################"
evalCmd "$PROG_NOM" $LINENO;

echo "#############################################"
evalCmd "$PROG_NOM --debug" $LINENO;

echo "#############################################"
evalCmd "$PROG_NOM --showVars" $LINENO;

echo "#############################################"
evalCmd "$PROG_NOM --update" $LINENO;

echo "#############################################"
evalCmd "$PROG_NOM --install" $LINENO;

echo "#############################################"
evalCmd "$PROG_NOM --local" $LINENO;

echo "#############################################"
libExiste="french_console"
evalCmd "$PROG_NOM  $libExiste"  $LINENO;

echo "#############################################"
libNonExiste="libNonExiste"
evalCmd "$PROG_NOM $libNonExiste" $LINENO;

echo "#############################################"
libNonExiste="libNonExiste"
evalCmd "$PROG_NOM $libNonExiste" $LINENO;

