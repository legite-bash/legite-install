#!/bin/bash
#maj:2020.01.10
#maj de ce fichier en local:cp /www/bash/legite-install/legite-install.etc.sh /etc/legite/legite-install.etc.sh
set -u;
# NE PAS MODIFIER IL SERA ECRASE A LA PROCHAINE SAUVEGARDE #

##############################################
# * FUNCTION d'INSTALLATION DES PROGRAMMES * #
##############################################
fonction_Test(){
    echo "fonction Test"
    if [ $IS_ROOT -eq 0 ];then
        echo "Appeller par root."
    else
        echo "Appeller par un user."
    fi
    notifsAdd "$FUNCNAME"
}
librairiesTbl['fonction_Test']=fonction_Test;

configure_clavier(){
    if [ $IS_ROOT -eq 0 ];then return; fi
    echo $BOLD${INFO}Configuration du clavier$NORMAL$NOBOLD
    evalCmd "dpkg-reconfigure console-setup"
    evalCmd "dpkg-reconfigure keyboard-configuration"
    evalCmd "loadkeys fr"
    notifsAdd "$FUNCNAME"
}
librairiesTbl['configure_clavier']=configure_clavier;

configure_french_console(){
    if [ $IS_ROOT -eq 0 ];then return; fi
    echo $BOLD${INFO}mise en francais du systeme$NORMAL$NOBOLD
    evalCmd "aptitude -y install task-french"
    notifsAdd "$FUNCNAME"
}
librairiesTbl['configure_french_console']=configure_french_console;

configure_french_desktop(){
    if [ $IS_ROOT -eq 0 ];then return; fi
    echo $BOLD${INFO}mise en francais du systeme$NORMAL$NOBOLD
    evalCmd "aptitude -y install task-french task-french-desktop"
    notifsAdd "$FUNCNAME" 
}
librairiesTbl['configure_french_desktop']=configure_french_desktop;

install_localpurge(){
    if [ $IS_ROOT -eq 0 ];then return; fi
    if [ ! -x /usr/sbin/localepurge ]
    then
        evalCmd "apt-get install localepurge"
        dpkg-reconfigure localepurge
        echo relancer le programme.
        notifsAdd "$FUNCNAME"
        exit #apres pour localepurge
    fi
}
librairiesTbl['install_localpurge']=install_localpurge;

install_aptitude(){
    if [ $IS_ROOT -eq 0 ];then return; fi
    if [ ! -x /usr/bin/aptitude ]
    then
        evalCmd "apt-get install aptitude"
        notifsAdd "$FUNCNAME"

    fi
}
librairiesTbl['install_aptitude']=install_aptitude;

install_youtubeDl(){
    if [ $IS_ROOT -eq 0 ];then return; fi
    echo ""
    echo "Installation de Youtube-dl"
    evalCmd "aptitude install curl"
    evalCmd "curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl"
    evalCmd "chmod a+rx /usr/local/bin/youtube-dl"
    notifsAdd "$FUNCNAME"
}
librairiesTbl['install_youtubeDl']=install_youtubeDl;

install_sublimeText(){
    if [ $IS_ROOT -eq 0 ];then return; fi
    echo "Installation de Sublime Text"
    if [ -e /usr/bin/subl ]
    then
        echo "$RED /usr/bin/subl est deja installe$NORMAL"
    else
        echo "https://www.sublimetext.com/docs/3/linux_repositories.html"
        evalCmd "wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add -"
        evalCmd "apt-get install apt-transport-https"
        evalCmd 'echo "deb https://download.sublimetext.com/ apt/stable/" | tee /etc/apt/sources.list.d/sublime-text.list'
        evalCmd "apt-get update && apt-get install sublime-text"
        notifsAdd "$FUNCNAME"
    fi
}
librairiesTbl['install_sublimeText']=install_sublimeText;

install_legralNet(){
    if [ $IS_ROOT -eq 0 ];then return; fi
    echo ""
    echo "creation des repertoires legralNet"

    if [ ! -e /www ]
    then
        evalCmd "mkdir -p /home/datas/www";
        evalCmd "chown -R 1000:1000 /home/datas/www"
        evalCmd "chmod -R 755 /home/datas/www"
        evalCmd "ln -s /home/datas/www/ /www"
    fi

    evalCmd "mkdir -p /legralNet/legral-serveur/legral-serveur-racine"
    evalCmd "mkdir -p /legralNet/legral-serveur/legral-serveur-www"
    evalCmd "mkdir -p /legralNet/legral-serveur/legral-serveur-mediatheques"
    evalCmd "mkdir -p /legralNet/legral-serveur/legral-serveur-docutheques"
    evalCmd "chown -R 1000:root /legralNet"

    evalCmd "mkdir -p /media/mediatheques/films"
    evalCmd "mkdir -p /media/mediatheques/mangas"
    evalCmd "mkdir -p /media/mediatheques/musiques"
    evalCmd "mkdir -p /media/mediatheques/series"
    evalCmd "chown -R 1000:root /media/mediatheques"

    evalCmd "mkdir -p /media/docutheques"
    evalCmd "mkdir -p /media/docutheques/images"
    evalCmd "mkdir -p /media/docutheques/textes"
    evalCmd "mkdir -p /media/docutheques/videos"
    evalCmd "chown -R 1000:root /media/docutheques"
    notifsAdd "$FUNCNAME"
}
librairiesTbl['install_legralNet']=install_legralNet;

install_lighttpd(){
    if [ $IS_ROOT -eq 0 ];then return; fi
    echo $BOLD${INFO}lighttpd php-cgi7_0$NORMAL$NOBOLD
    evalCmd "aptitude -y install lighttpd";
    evalCmd "mkdir -p /home/datas/www; ln -s /home/datas/www /www"
    evalCmd "service lighttpd status"
    notifsAdd "$FUNCNAME"
}
librairiesTbl['install_lighttpd']=install_lighttpd;

install_php_cgi7(){
    if [ $IS_ROOT -eq 0 ];then return; fi
    echo $BOLD${INFO}php-cgi7_0$NORMAL$NOBOLD
    evalCmd "aptitude -y install php-cgi php-pear";

    evalCmd "lighty-enable-mod fastcgi";
    evalCmd "lighty-enable-mod fastcgi-php";

    echo "http://pear.php.net/manual/en/package.fileformats.mp3-id.php";
    evalCmd "pear -y install MP3_Id";

    evalCmd "service lighttpd stop;service lighttpd start;service lighttpd status"
    notifsAdd "$FUNCNAME"
}
librairiesTbl['install_php_cgi7']=install_php_cgi7;

install_mySQL_php(){
    if [ $IS_ROOT -eq 0 ];then return; fi
    echo .
    echo $BOLD${INFO}mysql$NORMAL$NOBOLD

    echo ${INFO}Creation de la redirection$NORMAL
    evalCmd "chown -R mysql:mysql /home/datas/mysql/*"
    evalCmd "chmod -R 750 /home/datas/mysql/*";
    evalCmd "ln -s  /var/lib/mysql /home/datas/mysql"


    echo ${INFO}Installation$NORMAL
    evalCmd "aptitude -y install mariadb-server php-mysql";


    echo ${INFO}edition de la configuration$NORMAL
    evalCmd "vim /etc/mysql/my.cnf";

    echo ${INFO}rechargement des services$NORMAL
    evalCmd "service mysql stop;service mysql start;";
    evalCmd "service lighttpd stop;service lighttpd start";
    notifsAdd "$FUNCNAME"
}
librairiesTbl['install_mySQL_php']=install_mySQL_php;

install_mariadb_server_php(){
    if [ $IS_ROOT -eq 0 ];then return; fi
    echo .
    echo $BOLD${INFO}mysql$NORMAL$NOBOLD

    echo ${INFO}Creation de la redirection$NORMAL
    evalCmd "chown -R mysql:mysql /home/datas/mysql/*"
    evalCmd "chmod -R 750 /home/datas/mysql/*";
    evalCmd "ln -s  /var/lib/mysql /home/datas/mysql"

    echo ${INFO}Installation$NORMAL
    evalCmd "aptitude -y install mariadb-server php-mysql";


    echo ${INFO}edition de la configuration$NORMAL
    evalCmd "vim /etc/mysql/my.cnf";

    echo ${INFO}rechargement des services$NORMAL
    evalCmd "service mariadb stop;service mariadb start;";
    evalCmd "service lighttpd stop;service lighttpd start";
    notifsAdd "$FUNCNAME"
}
librairiesTbl['install_mariadb_server_php']=install_mariadb_server_php;

install_firefox(){
    if [ $IS_ROOT -eq 1 ];
    then
        local -r INSTALL_ROOT="/usr/share/";
    else
        local -r INSTALL_ROOT="$HOME/bin/";
        mkdir -p $INSTALL_ROOT;
    fi

    local ancienneVersion=$(/usr/local/bin/firefox -v);

    # - Telechargement - #
    if [ ! -f "/tmp/firefox.tar.gz" ];
    then
        wget -O /tmp/firefox.tar.gz "https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64&lang=fr";
    else
        echo "/tmp/firefox.tar.gz existe deja" 
        echo "${INFO} rm /tmp/firefox.tar.gz #pour supprimer une ancienne version${NORMAL}"
    fi

    # - Extraction - #
    if [ -d "$INSTALL_ROOT/firefox" ];
    then
        evalCmd "mv $INSTALL_ROOT/firefox $INSTALL_ROOT/firefox.old" ; # copie en backup du repertoire existant
    fi

    evalCmd "tar -xvf /tmp/firefox.tar.gz --directory=$INSTALL_ROOT"
    #evalCmd "rm /tmp/firefox.tar.gz"

    if [ ! -d $INSTALL_ROOT/firefox ];
    then
        echo "${WARN}La decompression n'a pas produit $INSTALL_ROOT/firefox{$NORMAL}"
        return 1;
    fi

    # - Creation du lien symbolique vers /usr/local/bin - #
    if [ $IS_ROOT -eq 1 ];
    then

        if [ -L "/usr/local/bin/firefox" ];
        then
            echo "${INFO}Le lien symbolique existe /usr/local/bin/firefox -> suppression$NORMAL" ;
            evalCmd "unlink /usr/local/bin/firefox"
        fi
        echo "Creation du lien symbolique vers /usr/local/bin"
        evalCmd "ln -s $INSTALL_ROOT/firefox/firefox /usr/local/bin/firefox"
    fi

    # - Affichage de la version - #
    if [ $IS_ROOT -eq 1 ];
    then
        newVersion=$(/usr/local/bin/firefox -v)
    else
        newVersion=$($INSTALL_ROOT/firefox/firefox -v)
    fi
    local notif="Mise a jours de la version (stable) $ancienneVersion vers $newVersion"
    echo "$notif";
    notifsAdd "$notif"
    return 0;

}
librairiesTbl['install_firefox']=install_firefox;


install_firefoxBeta(){
    if [ $IS_ROOT -eq 1 ];
    then
        local -r INSTALL_ROOT="/usr/share/";
    else
        local -r INSTALL_ROOT="$HOME/bin/";
        mkdir -p $INSTALL_ROOT;
    fi

    local ancienneVersion=$(/usr/local/bin/firefoxBeta -v);

    # - Telechargement - #
    if [ ! -f "/tmp/firefoxBeta.tar.gz" ];
    then
        wget -O /tmp/firefoxBeta.tar.gz "https://download.mozilla.org/?product=firefox-beta-latest-ssl&os=linux64&lang=fr";
    else
        echo "/tmp/firefoxBeta.tar.gz existe deja" 
        echo "${INFO} rm /tmp/firefoxBeta.tar.gz #pour supprimer une ancienne version${NORMAL}"
    fi

    # - Extraction - #
    if [ -d "/tmp/firefoxBeta" ];
    then
        evalCmd "rm -R /tmp/firefoxBeta" ;
    fi
    evalCmd "mkdir /tmp/firefoxBeta"
    evalCmd "tar -xvf /tmp/firefoxBeta.tar.gz --directory=/tmp/firefoxBeta"
    #evalCmd "rm /tmp/firefoxBeta.tar.gz"

    if [ ! -d /tmp/firefoxBeta/firefox ];
    then
        echo "${WARN}La decompression n'a pas produit /tmp/firefoxBeta/firefox$NORMAL"
        return 1;
    fi

    # - Suppression de l'ancien repertoire $INSTALL_ROOT - #
    if [ -d $INSTALL_ROOT/firefoxBeta ];
    then
         echo "${INFO}Repertoire d'installation pre-existant -> suppression"
         evalCmd "rm -R $INSTALL_ROOT/firefoxBeta"
    fi

    # - deplacment du repertoire temporaire vers $INSTALL_ROOT  - #
    evalCmd "mv /tmp/firefoxBeta/firefox /tmp/firefoxBeta/firefoxBeta"
    evalCmd "mv /tmp/firefoxBeta/firefoxBeta $INSTALL_ROOT"

    # - Suppression du repertoire temporaire d'installation - #
    evalCmd "rmdir /tmp/firefoxBeta"

    # - Creation du lien symbolique vers /usr/lib - #
    if [ $IS_ROOT -eq 1 ];
    then
        if [ -L "/usr/local/bin/firefoxBeta" ];
        then
            echo "${INFO}Le lien symbolique existe /usr/local/bin/firefoxBeta -> suppression$NORMAL" ;
            evalCmd "unlink /usr/local/bin/firefoxBeta"
        fi
        echo "Creation du lien symbolique vers /usr/local/bin"
        evalCmd "ln -s $INSTALL_ROOT/firefoxBeta/firefox /usr/local/bin/firefoxBeta"
    fi

    # - Affichage de la version - #
    if [ $IS_ROOT -eq 1 ];
    then
        newVersion=$(/usr/local/bin/firefoxBeta -v)
    else
        newVersion=$($INSTALL_ROOT/firefox/firefoxBeta -v)
    fi
    local notif="Mise a jours de la version (Beta) $ancienneVersion vers $newVersion"
    echo "$notif";
    notifsAdd "$notif"
    return 0;

}
librairiesTbl['install_firefoxBeta']=install_firefoxBeta;

install_firefoxDev(){
    if [ $IS_ROOT -eq 1 ];
    then
        local -r INSTALL_ROOT="/usr/share/";
    else
        local -r INSTALL_ROOT="$HOME/bin/";
        mkdir -p $INSTALL_ROOT;
    fi

    local ancienneVersion=$(/usr/local/bin/firefoxDev -v);

    # - Telechargement - #
    if [ ! -f "/tmp/firefoxDev.tar.gz" ];
    then
        wget -O /tmp/firefoxDev.tar.gz "https://download.mozilla.org/?product=firefox-devedition-latest-ssl&os=linux64&lang=fr";
    else
        echo "/tmp/firefoxDev.tar.gz existe deja" 
        echo "${INFO} rm /tmp/firefoxDev.tar.gz #pour supprimer une ancienne version${NORMAL}"
    fi

    # - Extraction - #
    if [ -d "/tmp/firefoxDev" ];
    then
        evalCmd "rm -R /tmp/firefoxDev" ;
    fi
    evalCmd "mkdir /tmp/firefoxDev"
    evalCmd "tar -xvf /tmp/firefoxDev.tar.gz --directory=/tmp/firefoxDev"
    #evalCmd "rm /tmp/firefoxDev.tar.gz"

    if [ ! -d /tmp/firefoxDev/firefox ];
    then
        echo "${WARN}La decompression n'a pas produit /tmp/firefoxDev/firefox$NORMAL"
        return 1;
    fi

    # - Suppression de l'ancien repertoire $INSTALL_ROOT - #
    if [ -d $INSTALL_ROOT/firefoxDev ];
    then
         echo "${INFO}Repertoire d'installation pre-existant -> suppression"
         evalCmd "rm -R $INSTALL_ROOT/firefoxDev"
    fi

    # - deplacement du repertoire temporaire vers $INSTALL_ROOT  - #
    evalCmd "mv /tmp/firefoxDev/firefox /tmp/firefoxDev/firefoxDev"
    evalCmd "mv /tmp/firefoxDev/firefoxDev $INSTALL_ROOT"

    # - Suppression du repertoire temporaire d'installation - #
    #echo $LINENO
    #evalCmd "rmdir /tmp/firefoxDev"

    # - Creation du lien symbolique vers /usr/lib - #
    if [ $IS_ROOT -eq 1 ];
    then
        if [ -L "/usr/local/bin/firefoxDev" ];
        then
            echo "${INFO}Le lien symbolique existe /usr/local/bin/firefoxDev -> suppression$NORMAL" ;
            evalCmd "unlink /usr/local/bin/firefoxDev"
        fi
        echo "Creation du lien symbolique vers /usr/local/bin"
        evalCmd "ln -s $INSTALL_ROOT/firefoxDev/firefox /usr/local/bin/firefoxDev"
    fi

    # - Affichage de la version - #
    if [ $IS_ROOT -eq 1 ];
    then
        newVersion=$(/usr/local/bin/firefoxDev -v)
    else
        newVersion=$($INSTALL_ROOT/firefox/firefoxDev -v)
    fi
    local notif="Mise a jours de la version (Dev)  $ancienneVersion vers $newVersion"
    echo "$notif";
    notifsAdd "$notif"
    return 0;

}
librairiesTbl['install_firefoxDev']=install_firefoxDev;


install_firefoxAll(){
    install_firefox
    install_firefoxBeta
    install_firefoxDev
    notifsAdd "$FUNCNAME"
}
librairiesTbl['install_firefoxAll']=install_firefoxAll;

install_visualCode(){
    if [ $IS_ROOT -eq 0 ];
    then
        echo "${WARN}le paquet .deb doit etre installé en root.$NORMAL"
        return 1;
    fi

    # - Telechargement - #
    if [ ! -f "/tmp/visualCode.deb" ];
    then
        wget -O /tmp/visualCode.deb "https://go.microsoft.com/fwlink/?LinkID=760868";
    else
        echo "/tmp/visualCode.deb existe deja"
        echo "${INFO} rm /tmp/visualCode.deb #pour supprimer une ancienne version${NORMAL}"
    fi

    # - Installation du paquet - #
    evalCmd "dpkg -i /tmp/visualCode.deb"
    evalCmd "code --user-data-dir=/tmp -v"
    notifsAdd "$FUNCNAME"
}
librairiesTbl['install_visualCode']=install_visualCode;

install_VSCodium(){
    echo "Doc: https://vscodium.com/"

    if [ $IS_ROOT -eq 0 ];
    then
        echo "${WARN}le paquet .deb doit etre installé en root.$NORMAL"
        return 1;
    fi

    # - Ajout de la clef gpg - #
    echo "Ajout de la clef gpg "
    if [ ! -f "/etc/apt/trusted.gpg.d/vscodium.gpg" ];
    then
        evalCmd "wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | dd of=/etc/apt/trusted.gpg.d/vscodium.gpg"
    else
        echo "Le clef gpg /etc/apt/trusted.gpg.d/vscodium.gpg existe deja"
        echo "${INFO} rm /etc/apt/trusted.gpg.d/vscodium.gpg #pour supprimer l'ancienne version${NORMAL}"
    fi

    # - Ajout du depot - #
    echo "Ajout du depot"
    if [ ! -f "/etc/apt/sources.list.d/vscodium.list" ];
    then
        evalCmd "echo 'deb https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs/ vscodium main' | tee --append /etc/apt/sources.list.d/vscodium.list"
    else    
        echo "Le depot /etc/apt/sources.list.d/vscodium.list existe deja"
        echo "${INFO} rm /etc/apt/sources.list.d/vscodium.list #pour supprimer l'ancienne version${NORMAL}"
    fi

    # - Installation du paquet - #
    evalCmd "aptitude update"
    evalCmd "aptitude install codium"
    notifsAdd "$FUNCNAME"
}
librairiesTbl['install_VSCodium']=install_VSCodium;


install_discord(){
    if [ $IS_ROOT -eq 0 ];then return; fi
    echo ""
    evalCmd "aptitude -y install libc6  libasound2  libatomic1  libgconf-2-4  libnotify4  libnspr4  libnss3  libstdc++6  libxss1  libxtst6  libappindicator1 libc++1";
    evalCmd "wget -O '/tmp/discord.deb' 'https://discord.com/api/download?platform=linux&format=deb'"
    evalCmd "dpkg -i /tmp/discord.deb"
    rm /tmp/discord.deb
    notifsAdd "$FUNCNAME"
}
librairiesTbl['install_discord']=install_discord;

install_libdvdcss(){
    if [ $IS_ROOT -eq 0 ];then return; fi
    echo "${INFO}installation de libdvd-pkd$NORMAL";
    echo "http://www.videolan.org/developers/libdvdcss.html";
    echo "Ce paquet va telecharger le libdvdcss puis le compiler et ensuite l'installer"
    evalCmd "aptitude -y install libdvd-pkg";
    evalCmd "dpkg-reconfigure libdvd-pkg";
    notifsAdd "$FUNCNAME"
}
librairiesTbl['install_libdvdcss']=install_libdvdcss;

install_virtualBox(){
    echo "Doc: https://www.virtualbox.org/wiki/Linux_Downloads"

    if [ $IS_ROOT -eq 0 ];
    then
        echo "${WARN}le paquet .deb doit etre installé en root.$NORMAL"
        return 1;
    fi

    # - Ajout de la clef gpg - #
    echo "Ajout de la clef gpg "
        evalCmd "wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | apt-key add -"
        evalCmd "wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | apt-key add -"

    # - Ajout du depot - #
    echo "Ajout du depot"
    sourceListFile="/etc/apt/sources.list.d/virtualbox.list";
    if [ ! -f "$sourceListFile" ];
    then
        evalCmd "echo 'deb https://download.virtualbox.org/virtualbox/debian buster contrib non-free' | tee --append $sourceListFile"
    else    
        echo "Le depot $sourceListFile existe deja"
        echo "${INFO} rm $sourceListFile #pour supprimer l'ancienne version${NORMAL}"
    fi

    # - Installation du paquet - #
    evalCmd "aptitude update"
    evalCmd "aptitude install virtualbox-6.1"
    notifsAdd "$FUNCNAME"
}
librairiesTbl['install_virtualBox']=install_virtualBox;


# Telecharge la derniere version netinstall en netboot amd64
# la copie sshfs est specifique a mon systeme il est facilement adaptable.
debianNetboot(){
    #appendSysmenu( $1=noyauPath $2=initrdPath)
    appendSysmenu(){
        menuPath="$destination/sysMenu/menus/subMenu-linuxInstall-auto.cfg";
        echo "" >> $menuPath;
        echo "LABEL $2" >> $menuPath;
        echo "    KERNEL images/linux/kernel/debian-installer/$1" >> $menuPath;
        echo "    INITRD images/linux/kernel/debian-installer/$2" >> $menuPath;
        echo "    APPEND priority=low vga=788 --" >> $menuPath;
    }

    echo $BOLD${INFO}Telechargement de la derniere version de Debian Netboot$NORMAL$NOBOLD
    declare -A debian;
    
    debianStable=10;
    debianUnstable=$(($debianStable + 1));
    #debianSid=$(($debianStable + 2));
    debian[10]="buster";
    debian[11]="bullseye";
    debian["sid"]="sid";
    #echo ${debian[$debianStable]}

    declare -r FTP_DIST="http://ftp.fr.debian.org/debian/dists";
    declare -r FTP_DIST_STABLE="$FTP_DIST/${debian[$debianStable]}/main/installer-amd64/current/images/netboot"
    declare -r FTP_DIST_UNSTABLE="$FTP_DIST/${debian[$debianUnstable]}/main/installer-amd64/current/images/netboot"
    declare -r FTP_DIST_SID="$FTP_DIST/sid/main/installer-amd64/current/images/netboot"

    echo "${INFO}Liste des FTP$NORMAL"
    echo $FTP_DIST_STABLE/
    echo $FTP_DIST_UNSTABLE/
    echo $FTP_DIST_SID/

    echo "${INFO}stable current AMD64 noyau$NORMAL"
    evalCmd "mkdir -p /tmp/debian/"
    cd /tmp/debian/

    # copier le kernel
    fichier="linux";
    if [ -f $fichier ]; then rm $fichier; fi

    wget $FTP_DIST_STABLE/debian-installer/amd64/$fichier
    d=$(date -r $fichier  '+%Y.%m.%d')
    cible="$d-debian-$debianStable-netboot-amd64-linux";
    evalCmd "cp $fichier $cible"
    linux=$cible;

    fichier="initrd.gz";
    if [ -f $fichier ]; then rm $fichier; fi
    wget $FTP_DIST_STABLE/debian-installer/amd64/$fichier
    d=$(date -r $fichier  '+%Y.%m.%d')
    cible="$d-debian-$debianStable-netboot-amd64-txt-initrd.gz";
    evalCmd "cp  $fichier $cible"
    initrdTxt=$cible;

    echo "${INFO}stable current AMD64 gtk$NORMAL"
    fichier="initrd.gz";
    if [ -f $fichier ]; then rm $fichier; fi
    wget  $FTP_DIST_STABLE/gtk/debian-installer/amd64/$fichier
    d=$(date -r $fichier  '+%Y.%m.%d')
    cible="$d-debian-$debianStable-netboot-amd64-gtk-initrd.gz";
    evalCmd "cp  $fichier $cible"
    initrdGTK=$cible;


    echo "${INFO}transfert des fichiers$NORMAL"

    # monter le repertoire distant
    destination="/legralNet/legral-serveur/legral-serveur-tftp";
    evalCmd "sshfs pi@192.168.0.202:/srv/tftp/ $destination -o follow_symlinks;"

    #tester s'il est bien monté
    if [ -f "$destination/pxelinux.0" ];
    then
        kernelRoot="$destination/images/linux/kernel/debian-installer"

        if [ ! -f "$kernelRoot/$linux" ];
        then
            evalCmd "cp $linux     $kernelRoot/$linux";
        else
            echo "${INFO}$kernelRoot/$linux existe deja$NORMAL"
        fi

        if [ ! -f "$kernelRoot/$initrdTxt" ];
        then
            evalCmd "cp $initrdTxt     $kernelRoot/$initrdTxt";
            appendSysmenu $linux $initrdTxt
        else
            echo "${INFO}$kernelRoot/$initrdTxt existe deja$NORMAL"
        fi
        appendSysmenu $linux $initrdTxt

        if [ ! -f "$kernelRoot/$initrdGTK" ];
        then
            evalCmd "cp $initrdGTK     $kernelRoot/$initrdGTK";
            appendSysmenu $linux $initrdGTK
        else
            echo "${INFO}$kernelRoot/$initrdGTK existe deja$NORMAL"
        fi
        appendSysmenu $linux $initrdGTK

        # demonter le repertoire distant
        evalCmd "fusermount -u $destination"
    else
        echo "${WARN}Le repertoire n'est pas monté.$NORMAL"
    fi
    notifsAdd "$FUNCNAME"
}
librairiesTbl['debianNetboot']=debianNetboot;
