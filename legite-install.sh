#!/bin/bash
set -u
#set -eE  # same as: `set -o errexit -o errtrace`
#style
#https://abs.traduc.org/abs-5.0-fr/ch32.html#unofficialst
#http://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html#The-Set-Builtin

declare -r SCRIPT_PATH=`realpath $0`;        # chemin  absolu complet du script rep + nom
declare -r SCRIPT_REP=`dirname $SCRIPT_PATH`;# repertoire absolu du script (pas de slash de fin)
declare -r VERSION="v2.0.0-2021.06.28";

if [ ! -f "$SCRIPT_REP/legite-lib.sh" ]
then
    echo "$SCRIPT_REP/legite-lib.sh introuvable -> quit"
    exit 1
fi
. $SCRIPT_REP/legite-lib.sh


#############
# variables #
#    init   #
#############


#############
# VARIABLES #
# DU PROJET #
#############
# --- variables des includes --- #
declare -r UPDATE_HTTP="$UPDATE_HTTP_ROOT/legite-install";
declare -i isLegiteLibsDw=0;               # la lib legite-lib est elle deja telechargé?


# #################### #
# FONCTIONS GENERIQUES #
# #################### #
usage (){
    if [ $argNb -eq 0 ];
    then
        echo "$SCRIPT_FILENAME [-d] [--showVars] [--showLibs] [--update] [--conf=fichier.sh] [programme]";
        echo "--update: update le programme legite-install.sh lui meme (via $UPDATE_HTTP)."
        echo "--conf: execute un fichier de configuration si existe."
        echo "Fichier de configurations:      (visible avec --showVars)."
        echo "programme: execute une fonction (visible avec --showLibs)."
        exit $E_ARG_NONE;
    fi
}


showVarsLocal(){
    showDebug "$FUNCNAME($@)";
}


showVarsPost(){
    showDebug "$FUNCNAME($*)"
    if [ $isShowVars -eq 1 ]
    then
        showVarsLocal
    fi
}


################
# MISE A JOURS #
################
#selfUpdateLocal(){
    # copier le modele de ./legite-lib.sh
#}

#############
# LOAD_CONF #
#############
loadConfsLocal(){
    showDebug "$FUNCNAME($*)"
    execFile "$CONF_REP/legite-push.cfg.sh"
}


# ################### #
# FONCTIONS DU PROJET #
# ################### #
#pushProg ("src" "dest")
pushBashToUpdates(){
    local src="${1:-""}";	local dest="${2:-"$src"}";
    if [ "$src" == "" ]; then return;fi
    evalCmd "scp -p /www/bash/$src pi@192.168.0.202:/www/www-sites/updates/$dest > /dev/null"
    if [ $? -ne 0 ]
    then
        echo "${WARN}Erreur avec le fichier: $dest$NORMAL"
    fi
}


#pushConfToPi ("src1" ["src2"])
pushConfToPi(){
    local src="${1:-""}";	local dest="${2:-"$src"}";
    if [ "$src" == "" ]; then return;fi
    local srcPath="/home/pascal/.legite/$src"
    if [ ! -f "$srcPath" ]
    then
        echo $WARN"$srcPath inextant."$NORMAL
        return
    fi        

    evalCmd "scp -p '$srcPath' pi@192.168.0.202:/home/pi/.legite/$dest > /dev/null"

    if [ $? -ne 0 ]
    then
        echo $WARN"Erreur avec le fichier: $dest"$NORMAL
    fi
}


##########################
# GESTION DES LIBRAIRIES #
##########################


# ########## #
# PARAMETRES #
# ########## #
TEMP=$(getopt \
    --options v::dVhnS \
    --long help,version,verbose::,debug,update,showVars,conf::,showLibs\
,none \
    -- "$@")
eval set -- "$TEMP"

while true
do
    if [ $isTrapCAsk -eq 1 ];then break;fi
    argument=${1:-""}
    parametre=${2:-""}
    #echo "argument:'$argument', parametre='$parametre'"

    case "$argument" in

        # - fonctions generiques - #
        -h|--help) usage; exit 0; shift ;;
        -V|--version) echo "$VERSION"; exit 0; shift ;;
        -v|--verbose)
            case "$2" in
                "") verbosity=1; shift 2 ;;
                *)  #echo "Option c, argument \`$2'" ;
                verbosity=$2; shift 2;;
            esac
            ;;

        # - Debug - #
        -d|--debug) isDebug=1;isShowVars=1;shift;  ;;
        --showVars) isShowVars=1; shift  ;;

        # - Mise a jours - #        
        --update)   isUpdate=1;   shift ;;

        # - Chargement d'un fichier de configuration - #
        --conf)     CONF_PSP_PATH="$parametre";  shift 2; ;;

        # - Librairies - #
        --showLibs) isShowLibs=1;         shift ;;

        #--) # parcours des arguments supplementaires
        --)
            if [ -z "$parametre" ]
            then
                shift;
                break;
            fi
            #echo "--)$argument,$parametre"
            librairie="$parametre";
            librairiesCall[$librairiesCallIndex]="$librairie"
            ((librairiesCallIndex++))
            shift 2;
            ;;

        *)
            if [ -z "$argument" ]
            then
                shift 1;
                break;
            fi
            #echo "*)$argument,$parametre"
            librairie="$argument";
            librairiesCall[$librairiesCallIndex]="$librairie"
            ((librairiesCallIndex++))
            shift 1;
            ;;

        esac
done


########
# main #
########
#clear;
echo "${INFO}$SCRIPT_PATH $VERSION$NORMAL";

usage;
selfUpdate;
showVars;
loadConfs
showLibs;
execLibrairiesCall

#echo "Résumé: "
notifsShow;
erreursShow;

showVarsPost
if [ $isTrapCAsk -eq 1 ];then exit E_CTRL_C;fi
exit 0