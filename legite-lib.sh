#!/bin/bash

#declare -r SCRIPT_PATH=$(realpath $0);         # chemin  absolu complet du script rep + nom
#declare -r SCRIPT_REP=$(dirname $SCRIPT_PATH); # repertoire absolu du script (pas de slash de fin)
declare -r SCRIPT_FILENAME=${0##*/};          # /path/nom.pasext.ext -> nom.pasext.ext
declare -r SCRIPT_NAME=${SCRIPT_FILENAME%.*}  # nom.pasext.ext -> nom.pasext # uniquement le nom
declare -r SCRIPT_EXT=${SCRIPT_FILENAME##*.}  # nom.pasext.ext -> ext        # uniquement l'extention


################
# COLORISATION #
################
declare -r RESET_COLOR=$(tput sgr0)
declare -r BOLD=`tput smso`
declare -r NOBOLD=`tput rmso`

declare -r BLACK=`tput setaf 0`
declare -r RED=`tput setaf 1`
declare -r GREEN=`tput setaf 2`
declare -r YELLOW=`tput setaf 3`
declare -r CYAN=`tput setaf 4`
declare -r MAGENTA=`tput setaf 5`
declare -r BLUE=`tput setaf 6`
declare -r WHITE=`tput setaf 7`

declare -r NORMAL=$WHITE
declare -r INFO=$BLUE
declare -r CMD=$YELLOW
declare -r WARN=$RED
declare -r TITRE1=$GREEN
declare -r TITRE2=$MAGENTA
declare -r DEBUG_COLOR=$MAGENTA


####################
# CODES DE SORTIES #
####################
#http://www.gnu.org/software/bash/manual/html_node/Exit-Status.html#Exit-Status
#https://abs.traduc.org/abs-5.0-fr/apd.html#exitcodesref
#https://tldp.org/LDP/abs/html/exitcodes.html

declare -r -i E_MISC=1
declare -r -i E_INTERNE=2
# 64..113: USER EXIT
declare -r -i E_ARG_NONE=65
declare -r -i E_ARG_BAD=66
declare -r -i E_REQUIRE=67;
declare -r -i E_UPDATE=68;            # sorite apres une update
#>=129.255 FATAL_ERROR
declare -r -i E_CMD_CANT_EXEC=126
declare -r -i E_CMD_NOT_FOUND=127
declare -r -i E_EXIT_BAD=128          # valeur d'exit invalide
declare -r -i E_CTRL_C=130
declare -r -i E_EXIT_EOR=255          # error Of Range ?


#############
# VARIABLES #
#  GLOBALES #
#############
# --- variables des includes --- #
declare -r UPDATE_HTTP_ROOT="http://updates.legite.org";

declare CONF_REP="$HOME/.legite";
declare CONF_ORI_PATH="$CONF_REP/$SCRIPT_NAME.ori.sh";
declare CONF_SRC_PATH="$SCRIPT_REP/$SCRIPT_NAME.ori.sh";
declare CONF_FILENAME="$SCRIPT_NAME.cfg.sh";
declare CONF_USR_PATH="$CONF_REP/$CONF_FILENAME";
declare CONF_PSP_PATH="";   # fournis en parametre

# --- Definir si root ou user --- #
[ "$(id -u)" == "0" ] && declare -r -i IS_ROOT=1 || declare -r -i IS_ROOT=0;

# --- tag d'interuption --- #
declare -i isTrapCAsk=0;     # demande d'interuption par control C

# --- variables generiques --- #
declare -r -i argNb=$#;
declare argument;              # argument actuel ($1)
declare parametre;             # parametre actuel ($2)
declare -i isUpdate=0;         # demande de update
declare -i verbosity=0;        # niveau de verbosité
declare -i isDebug=0;
declare -i isShowVars=0;       # montrer les variables
declare -i isShowLibs=0;       #


#################
# NOTIFICATIONS #
#################
declare -a notifsPile;              # tableau contenant la pile de notifications
declare -i notifsIndex=0;       # index utilisé pour le remplissage de la pile de notifs

#notifsAdd($txt) # ajoute une notif dans la pile de notifs
notifsAdd(){
    local notif="$1"
    if [ -z "$notif" ]
    then
        return 1;
    fi

    notifsPile[$notifsIndex]="$notif"
    ((notifsIndex++))
    return 0;
}

#notifsShow($separateur="\n") # affiche la pile des notifs
notifsShow(){
    if [ -z "${notifsPile[*]}" ];then return 1;fi
    local separateur=${1:-"\n"}
    echo -e "\n"$TITRE1"Notifs($notifsIndex)"$NORMAL
    for notif in "${notifsPile[@]}"
    do
        echo -ne "$notif$separateur";
    done
    return 0;
}


###########
# ERREURS #
###########
declare -a erreursPile;              # tableau contenant la pile de notifications
declare -i erreursPileIndex=0;       # index utilisé pour le remplissage de la pile des erreur

#erreursAdd($txt) # ajoute une notif dans la pile de notifs
erreursAdd(){
    local erreur="$1"
    if [ -z "$erreur" ];then return 1; fi

    erreursPile[$erreursPileIndex]="$erreur"
    ((erreursPileIndex++))
    return 0;
}

#erreursShow($separateur="\n") # affiche la pile des erreurs
erreursShow(){
    if [ -z "${erreursPile[*]}" ];then return 1;fi
    local separateur=${1:-"\n"}
    echo -e "\n"$WARN"Erreurs($erreursPileIndex)"$NORMAL
    for erreur in "${erreursPile[@]}"
    do
        echo -ne "$erreur$separateur";
    done
    return 0;
}


############
# TRAP_ERR #
############
declare -a trapErrPile;              # tableau contenant la pile de notifications
declare -i trapErrPileIndex=0;       # index utilisé pour le remplissage de la pile des erreur

#trapErrAdd($txt) # ajoute une notif dans la pile
trapErrAdd(){
    local valeur="$1"
    if [ -z "$valeur" ];then return 1; fi

    trapErrPile[$trapErrPileIndex]="$valeur"
    ((trapErrPileIndex++))
    return 0;
}

#trapErrShow($separateur="\n") # affiche la pile
trapErrShow(){
    if [ -z "${trapErrPile[*]}" ]; then return 1; fi

    echo -e "\n"$TITRE1"Erreurs($erreursPileIndex)"$NORMAL
    local separateur=${1:-"\n"}
    for valeur in "${erreursPile[@]}"
    do
        echo -ne "$valeur$separateur";
    done
    return 0;
}


#########
# DEBUG #
#########
#evalCmd($cmd $ligneNu $txt)
evalCmd(){
    if [ $# -gt 0 ]
    then
        local ligneNu=${2:-""}
        local txt=${3:-""}

        echo "${DEBUG_COLOR}$ligneNu$CMD$1$NORMAL$txt";
        eval "$1";
        return $?
    fi
}


#showDebug(texte pre post)
showDebug(){
    if [ $isDebug -eq 1 -a $# -gt 0 ]
    then
        texte="$1";
        local pre=${2:-""}
        local post=${3:-""}

        echo "${DEBUG_COLOR}$pre$texte$post$NORMAL"
    fi
}

displayVar (){
    #showDebug "$FUNCNAME($*)"
    local out="";
    while [ $# -gt 1 ]
    do
        out="$out $INFO $1:$NORMAL'$2'"
        shift 2;
    done
    echo "$out"
}


# ##### #
# TRAPS #
# ##### #
#http://www.gnu.org/software/bash/manual/html_node/Bourne-Shell-Builtins.html#Bourne-Shell-Builtins
#trap -l #trap --help
#https://linuxhint.com/bash_error_handling/
#https://tldp.org/LDP/Bash-Beginners-Guide/html/sect_12_02.html
trap 'echo "$LINENO";ctrl_c' INT

function ctrl_c() {
    showDebug "$FUNCNAME($@) ** Trapped CTRL-C"
    isTrapCAsk=1;
}

#trap 'echo "$WARN"catch: ligne=$LINENO code_retour=$?"$NORMAL"' ERR


################
# MISE A JOURS #
################
# telecharge la derniere version en supprimant la version local
#updateProg ("src" "dest") # chemin absolu
updateProg(){
    showDebug "$FUNCNAME($*)"
    if [ $isUpdate -eq 1 ]
    then
        src="$1"; dest="$2"
        echo "${INFO}Update de $src$NORMAL"
        #if [ $IS_ROOT -eq 1 ]
        #then
            local dbak=$(date +'%m-%d-%HH%Mm');
            evalCmd "cp '$dest' '$dest-$dbak.bak'";     # Fait une sauvegarde
            evalCmd "rm '$dest'";

            evalCmd "wget -O '$dest' '$src'"
            evalCmd "chmod +x '$dest'"
            if [ $? -ne 0 ]
            then
                echo $WARN"Erreur lors de l'installation de $dest."$NORMAL
                evalCmd "mv '$dest.bak' '$dest'"
            fi

        #else
        #    echo "${WARN}Seul root peut updater (ecrire dans /usr/local/bin/, /root/.legite/)"
        #fi
    fi
}

# - telecharge et installe la derniere version en remplacant le script qui a lancer l'update - #
selfUpdate (){
    if [ $isUpdate -eq 1 ]
    then
        echo "mise a jours du programme";

        cd "$SCRIPT_REP";
        updateProg "$UPDATE_HTTP/$SCRIPT_FILENAME" "./$SCRIPT_FILENAME"
        cd -        
        if [ ! -d "$CONF_USR_ROOT" ];then mkdir "$CONF_USR_ROOT";fi
        updateProg "$UPDATE_HTTP/CONF_ORI_FILENAME"  "$CONF_ORI_PATH"
        updateProg "$UPDATE_HTTP/CONF_USR_FILENAME"  "$CONF_USR_PATH"

    selfUpdateLocal
    exit $E_UPDATE;
    fi
}

# a surcharger par le projet
selfUpdateLocal(){
    if [ $isUpdate -eq 1 ]
    then
        showDecho "mise a jours specifique au programme";

        if [ $IS_ROOT -eq 1  ]
        then
            showDebug ""
        fi

    fi
}


# #### #
# EXEC #
# #### #
# creer et execute les fichiers locaux
# LocalFileExec(localName){
execFile(){
    showDebug "$FUNCNAME($*)";
    local localName="$1";
    if [ -z "$localName" ]; then return; fi
    if [ -f "$localName" ]
    then
        if [ ! -x "$localName" ];then evalCmd "chmod +x $localName;"; fi
        #echo "nano $localName # Pour modifier le fichier"
        evalCmd ". $localName";
    else
        echo "  ${WARN}$localName n'existe pas.$NORMAL"
    fi
}


# ######## #
# SHOWVARS #
# ######## #
showVars(){
    showDebug "$FUNCNAME($*)"
    if [ $isShowVars -eq 1 ]
    then
        #displayVar " argNb=$argNb"
        displayVar "VERSION" "$VERSION"
        displayVar "IS_ROOT  " "$IS_ROOT" "isUpdate" "$isUpdate"
        displayVar "verbosity" "$verbosity" "isDebug" "$isDebug" "isShowVars" "$isShowVars" "isShowLibs" "$isShowLibs"

        displayVar "UPDATE_HTTP_ROOT" "$UPDATE_HTTP_ROOT";
        displayVar "UPDATE_HTTP     " "$UPDATE_HTTP";
        displayVar "CONF_REP        " "$CONF_REP";
        displayVar "CONF_SRC_PATH   " "$CONF_SRC_PATH";
        displayVar "CONF_FILENAME   " "$CONF_FILENAME";
        displayVar "CONF_ORI_PATH   " "$CONF_ORI_PATH";
        displayVar "CONF_USR_PATH   " "$CONF_USR_PATH";
        displayVar "CONF_PSP_PATH   " "$CONF_PSP_PATH";

        displayVar "librairiesCall[*]" "${librairiesCall[*]}"
        displayVar "librairiesTbl[*]" "${librairiesTbl[*]}"

        showVarsLocal
    fi
}

# a surcharger dans le projet
showVarsLocal(){
    showDebug "$FUNCNAME($@)";
}


# a surcharger dans le projet
showVarsPost(){
    showDebug "$FUNCNAME($*)"
    if [ $isShowVars -eq 1 ]
    then
        showVarsLocal
    fi
}


##########################
# GESTION DES LIBRAIRIES #
##########################
declare -A librairiesCall;       # (array) liste des librairies appellées
declare -i librairiesCallIndex=0 # index pour construire le tableau
declare    librairie="";         # librairie en cours
declare -A librairiesTbl;
default(){
    showDebug "@FUNCNAME'$*)"
}
librairiesTbl['default']=default;
showLibs(){
    if [ $isShowLibs -eq 1 ]
    then
        local sorted='';
        echo "Librairies possibles:";
        IFS=$'\n';
        sorted=($(sort <<<"${librairiesTbl[*]}"));
        unset IFS

        local out="";
        for value in "${sorted[@]}";
        do
            out="$out $value"
        done;
        echo "$out";
    fi
}


# renvoie:
# -1 si pas d'argument
# -2 si lib == ""
#  1 si lib existe
#  0 autre (lib non existe)

isLibExist(){
    if [ $# -eq 0 ]; then return -1;fi
    local lib="$1"
    #displayVar "lib" "$lib"
    if [ "$lib" == "" ];then return -2;fi
    for value in "${librairiesTbl[@]}";
    do
        if [ "$value" == "$lib" ]; then return 1; fi
    done;
    return 0;
}


execLibrairiesCall(){
    for librairieNu in {0..10} #$librairiesCallIndex}
    do
    if [ $isTrapCAsk -eq 1 ];then break;fi
        echo ""
        local librairie=${librairiesCall[$librairieNu]:-""}
        #displayVar "librairieNu/librairiesCallIndex/nom" "$librairieNu/$librairiesCallIndex/$librairie";

        isLibExist "$librairie"
        local -i errNu=$?
        #displayVar "errNu" "$errNu"
        case $errNu in
        1)
            showDebug "Lancement de $librairie";
            evalCmd "$librairie";
            ;;
        0|1)
            echo $WARN"librairie $librairie: inexistante!"$NORMAL
            ;;
        2)
            showDebug " Pas de librairie";
            break
            ;;
        254)
            break
        esac

    done
}


#############
# LOAD CONF #
#############
loadConfs(){
    showDebug "$FUNCNAME($*)"
    execFile "$CONF_ORI_PATH"
    execFile "$CONF_USR_PATH"
    execFile "$CONF_PSP_PATH"
    execFile "$CONF_SRC_PATH"   # rep script du fichier ori (pour)
    loadConfsLocal
}

# a surcharger par le projet
loadConfsLocal(){
    showDebug "$FUNCNAME($*)"
    #execFile "$SCRIPT_REP/lib.cfg.sh
}

#############
# NORMALIZE #
#   TEXT    #
#############
declare normalizeText="";               # variable de resultat de la fonction normalizeTexte()
# prend une chaine et renvoie une chaine en normalisant les caracteres
getNormalizeText(){
    if [ $# -eq 0 ];then return 1; fi

    normalizeText="$1"; # global

    normalizeText=$(echo $normalizeText | tr -d '?');
    normalizeText=$(echo $normalizeText | tr -d '!');
    normalizeText=$(echo $normalizeText | tr -d '$');
    normalizeText=$(echo $normalizeText | tr -d '@');
    #normalizeText=$(echo $normalizeText | tr -d '#');

    normalizeText=$(echo $normalizeText | sed 's/[âàä]/a/g');
    normalizeText=$(echo $normalizeText | sed 's/[ÂÀÂ]/a/g');
    normalizeText=$(echo $normalizeText | sed 's/[éèêë]/e/g');
    normalizeText=$(echo $normalizeText | sed 's/[ÉÈÊË]/E/g');
    normalizeText=$(echo $normalizeText | sed 's/[îï]/i/g');
    normalizeText=$(echo $normalizeText | sed 's/[ÎÏ]/I/g');
    normalizeText=$(echo $normalizeText | sed 's/[ôö]/o/g');
    normalizeText=$(echo $normalizeText | sed 's/[ÔÖ]/O/g');
    normalizeText=$(echo $normalizeText | sed 's/[ûüù]/u/g');
    normalizeText=$(echo $normalizeText | sed 's/[ÛÜÙ]/U/g');
    normalizeText=$(echo $normalizeText | sed 's/[çÇ]/c/g');

    normalizeText=$(echo $normalizeText | tr "'" '_');
    normalizeText=$(echo $normalizeText | tr ":" '-');
    normalizeText=$(echo $normalizeText | sed 's/  */_/g');
    normalizeText=$(echo $normalizeText | tr "œ" 'oe');
    normalizeText=$(echo $normalizeText | tr "Œ" 'OE');

    normalizeText=$(echo $normalizeText | tr '"' '-');
    normalizeText=$(echo $normalizeText | tr ']' '-');
    normalizeText=$(echo $normalizeText | sed -e "s/[~–«»#^:\[\`\{\}\(\)\|\#,=]/-/g");
    normalizeText=$(echo $normalizeText | sed -e "s/@=]/-/g");
    normalizeText=$(echo $normalizeText | sed -e "s/[ '’°]/_/g");

    normalizeText=$(echo $normalizeText | tr '&' '_et_');

    normalizeText=$(echo $normalizeText | sed -e 's/--*\././g');
    normalizeText=$(echo $normalizeText | sed -e 's/__*\././g');


    normalizeText=$(echo $normalizeText | sed 's/__*-__*/-/g');
    normalizeText=$(echo $normalizeText | sed 's/_-/-/g');
    normalizeText=$(echo $normalizeText | sed 's/-_/-/g');

    normalizeText=$(echo $normalizeText | sed 's/__*/_/g');
    normalizeText=$(echo $normalizeText | sed 's/--*/-/g');
}


############
#   FILE   #
# HORODATE #
############
declare fileHorodated="";
fileHorodate(){
    #local d=$(date +"%Y-%m-%d-%H-%M")
    fileHorodated="$1-"$(date +"%Y-%m-%d-%H-%M")
}